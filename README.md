`checkist` is a curses program for using a checklist written in
CommonMark/Markdown.

For example, suppose that rhaglen-bore.md contains the following text:

```
* [ ] Shower.
* [ ] Apply deodorant.
* [ ] Apply sunscreen.
* [ ] Eat breakfast.
* [ ] Brush teeth.
```

`checkist` will allow you to check off items as they are performed.
