// Copyright 2017 Matt Kraai

// This file is part of checkist.

// checkist is free software: you can redistribute it and/or modify it under the terms of the GNU
// Affero General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.

// checkist is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with checkist.  If
// not, see <http://www.gnu.org/licenses/>.

#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]

#[macro_use]
extern crate clap;
#[macro_use]
extern crate error_chain;
extern crate pancurses;
extern crate pulldown_cmark;

use std::fs::File;
use std::io::Read;
use std::process;

use clap::{App, Arg};
use pancurses::{curs_set, endwin, initscr, noecho, Attribute, Input, Window};
use pulldown_cmark::{Event, Parser, Tag};

struct Item {
    checked: bool,
    description: String,
}

error_chain!{}

fn main() {
    if let Err(e) = run() {
        eprintln!("Error: {}", e);

        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }

        process::exit(1);
    }
}

fn parse_items(contents: &str) -> Vec<Item> {
    let mut items = Vec::new();
    let mut in_item = false;
    let mut description = None;
    for event in Parser::new(contents) {
        match event {
            Event::Start(Tag::Item) => {
                in_item = true;
                description = None;
            }
            Event::Text(item) => if in_item {
                match description {
                    None => if item.starts_with("[ ] ") {
                        description = Some(item[4..].to_string());
                    } else {
                        in_item = false;
                    },
                    Some(ref mut description) => {
                        description.push(' ');
                        description.push_str(&item);
                    }
                }
            },
            Event::End(Tag::Item) => {
                in_item = false;
                if let Some(description) = description {
                    items.push(Item {
                        checked: false,
                        description: description,
                    });
                }
                description = None;
            }
            _ => {}
        }
    }
    items
}

fn get_lines(window: &Window) -> usize {
    (window.get_max_y() - 1) as usize
}

fn display_items(window: &Window, items: &[Item], selection: usize) {
    let lines = get_lines(&window);
    let page = selection / lines;
    let start = page * lines;
    window.erase();
    for (line_number, item) in items[start..items.len().min(start + lines)]
        .iter()
        .enumerate()
    {
        if start + line_number == selection {
            window.attron(Attribute::Bold);
        }
        let line = format!(
            "[{}] {}",
            if item.checked { 'x' } else { ' ' },
            item.description
        );
        window.mvprintw(
            line_number as i32,
            0,
            &line[..line.len().min(window.get_max_x() as usize)],
        );
        if start + line_number == selection {
            window.attroff(Attribute::Bold);
        }
    }
    window.refresh();
}

fn search(items: &[Item], selection: &mut usize, query: &str) {
    let mut found = false;
    for (index, item) in items.iter().enumerate().skip(*selection + 1) {
        if item.description.contains(query) {
            found = true;
            *selection = index;
            break;
        }
    }
    if !found {
        for (index, item) in items.iter().enumerate().take(*selection + 1) {
            if item.description.contains(query) {
                *selection = index;
                break;
            }
        }
    }
}

fn run() -> Result<()> {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .arg(Arg::with_name("FILE").required(true))
        .get_matches();
    let path = matches.value_of("FILE").unwrap();
    let mut file = File::open(&path).chain_err(|| format!("Unable to open '{}'", path))?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .chain_err(|| format!("Unable to read from '{}'", path))?;
    let mut items = parse_items(&contents);
    let window = initscr();
    curs_set(0);
    noecho();
    window.keypad(true);
    let mut selection = 0;
    let mut saved_query = None;
    loop {
        display_items(&window, &items, selection);
        match window.getch() {
            Some(Input::Character(' ')) | Some(Input::Character('\n')) => {
                items[selection].checked = !items[selection].checked
            }
            Some(Input::Character('/')) => {
                let lines = get_lines(&window) as i32;
                window.mvaddstr(lines, 0, "Search for: ");
                let mut query = String::new();
                loop {
                    match window.getch().unwrap() {
                        Input::KeyBackspace => if !query.is_empty() {
                            query.pop();
                            window.mv(lines, window.get_cur_x() - 1);
                            window.addch(' ');
                            window.mv(lines, window.get_cur_x() - 1);
                        },
                        Input::Character('\n') => break,
                        Input::Character(c) => {
                            window.addch(c);
                            query.push(c);
                        }
                        _ => {}
                    }
                }
                search(&items, &mut selection, query.as_str());
                saved_query = Some(query);
            }
            Some(Input::Character('j')) | Some(Input::KeyDown) => if selection < items.len() - 1 {
                selection += 1;
            },
            Some(Input::Character('k')) | Some(Input::KeyUp) => if 0 < selection {
                selection -= 1;
            },
            Some(Input::Character('n')) => if let Some(ref query) = saved_query {
                search(&items, &mut selection, query);
            },
            Some(Input::Character('q')) => break,
            Some(Input::Character('x')) => items[selection].checked = true,
            Some(Input::Character('Z')) | Some(Input::KeyPPage) => {
                let lines = get_lines(&window);
                selection = if selection >= lines {
                    selection / lines * lines - 1
                } else {
                    0
                }
            }
            Some(Input::Character('z')) | Some(Input::KeyNPage) => {
                let lines = get_lines(&window);
                selection = ((selection / lines + 1) * lines).min(items.len() - 1);
            }
            _ => {}
        }
    }
    endwin();
    Ok(())
}
